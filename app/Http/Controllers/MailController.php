<?php

namespace App\Http\Controllers;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class MailController extends Controller
{
    function index(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'surname'  =>  'required',
            'cellPhone' =>  'required',
            'email'  =>  'required|email',
            'subject' => 'required',
            'company' => 'required',
            'message' => 'required'
           ]);
           
         $data = array(
              'name'      =>  $request->input('name'),
              'surname'   =>   $request->input('surname'),
              'cellPhone'      =>  $request->input('cellPhone'),
              'company'      =>  $request->input('company'),
              'officePhone'      =>  $request->input('officePhone'),
              'message'      =>  $request->input('message'),
              'company'      =>  $request->input('company'),
              'subject'      =>  $request->input('subject')
          );
          $email = $request->input('email');
      
        Mail::to($email)->send(new SendMail($data));
      
        return 200;
    }
}
