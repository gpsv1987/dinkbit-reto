<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('index');});
Route::get('/About', function () {return view('index');});
Route::get('/Services', function () {return view('index');});
Route::get('/Faq', function () {return view('index');});
Route::get('/Contact', function () {return view('index');});
Route::get('/Qoute', function () {return view('index');});
Route::post('/send', [MailController::class, 'index']);


