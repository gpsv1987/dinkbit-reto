import React, { useState, useEffect, Fragment } from 'react';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';
import RangeSlider from 'react-bootstrap-range-slider';
const QouteApp = () => {
  const [amountSwitch, setAmountSwitch] = useState(5000);
  const [periodSwitch, setPeriodSwitch] = useState(8);
  const onChangeAmount = e => {

    setAmountSwitch(e.target.value);
    onsole.log(e);
  }
  let data = [];
  const onChangePeriod = changeEvent => {
    setPeriodSwitch(changeEvent.target.value);
    for (let i = 1; i <= periodSwitch; i++) {
      data.push([{ id: periodSwitch }]);
    }
  }
  const [value, setValue] = useState(0);
  return (
    <div className="w-full py-4 bg-white shadow-lg rounded-lg my-32 ">

      <div className="px-4">
        <p className="text-gray-400 text-sm font-normal">Arrastre los botones de abajo para el monto que desea pedir prestado y en cuantas quincenas</p>
        <hr className="my-2" />
        <p className="text-gray-400 text-sm font-normal pb-4">MONTO</p>
        <RangeSlider
          min={5000} max={100000} step={1000}
          tooltipPlacement="top"
          size="sm"
          value={amountSwitch}
          onChange={changeEvent => setAmountSwitch(changeEvent.target.value)}
        />

        <div className="flex flex-wrap w-full pb-3">
          <p className="text-gray-400 text-sm font-normal text- w-6/12">$5000</p>
          <p className="text-gray-400 text-sm font-normal text-right w-6/12">$100,000</p>
        </div>


        <p className="text-gray-400 text-sm font-normal pb-4">QUINCENAS</p>
        <RangeSlider
          min={8} max={48} step={2}
          tooltipPlacement="top"
          size="sm"
          value={periodSwitch}
          onChange={onChangePeriod}
        />

        <div className="flex flex-wrap w-full pb-4">
          <p className="text-gray-400 text-sm font-normal text- w-6/12">8</p>
          <p className="text-gray-400 text-sm font-normal text-right w-6/12">48</p>
        </div>
      </div>
      <div className="bg-gray-100 flex flex-wrap w-full">
        <p className="text-gray-600 text-sm font-normal text-center w-4/12">MONTO<br></br><label className="text-blue-400">$ {amountSwitch}</label></p>
        <p className="text-gray-600 text-sm font-normal text-center w-4/12">QUINCENAS<br></br><label className="text-blue-400">{periodSwitch}</label></p>
        <p className="text-gray-600 text-sm font-normal text-center w-4/12">PAGOS<br></br><label className="text-blue-400">c</label></p>
        <button className="bg-green-500 hover:bg-green-600 w-full text-center text-white font-normal text-lg  mx-2 py-2">OBTENGA SU DINERO AHORA</button>
        <p className="text-gray-400 text-sm font-normal text-center w-full py-4">VER TABLA DE AMORTIZACIÓN</p>
      </div>
      <div className="container px-3 pt-2">
        <table className="">
          <thead className="text-gray bg-gray-200 flex text-gray-400 w-full mr-2">
            <tr className="w-full mb-4">
              <th className="w-1/12 text-center text-xs px-1">No. de pago</th>
              <th className="w-1/6 text-center text-xs px-1">Saldo Insoluto</th>
              <th className="w-1/6 text-center text-xs px-1">Amortización</th>
              <th className="w-1/6 text-center text-xs px-1">Interés</th>
              <th className="w-1/6 text-center text-xs px-1">IVA</th>
              <th className="w-1/6 text-center text-xs px-1">Pago</th>
            </tr>
          </thead>
          <tbody className="bg-grey-light flex flex-col items-center justify-between w-full">
            <tr className="flex w-full mb-4">
              <td className="w-1/6">10</td>
              <td className="w-1/6">1123</td>
              <td className="w-1/6">1789</td>
              <td className="w-1/6">15</td>
              <td className="w-1/6">145</td>
              <td className="w-1/6">1789</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default QouteApp;