import React from 'react';
const Faq = () => {
    const logoCard = "public/assets/img/Headers/header-faqs.jpg";
    return (
        <div className="min-h-full font-body">
            <div className="imageBack flex flex-col">
                <div className="h-screen bg-cover relative flex items-center justify-center h-full bg-center bg-no-repeat max-h-screen min-h-full"
                    style={{ backgroundImage: `url(${logoCard})` }} >
                    <div className="z-10 container text-center px-3 lg:px-0 max-w-screen-lg">
                        <p className="lg:text-xl md:text-xl text-base text-white">Lorem Ipsum</p>
                        <p className="leading-normal text-base md:text-xl lg:text-2xl mb-8 text-white font-medium">
                            Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.
                        </p>
                    </div>
                </div>
                <section className=" text-gray-200 bg-white">
                    <div className="max-w-screen-lg mx-auto px-5 py-24">
                        <div className="flex flex-wrap w-full mb-20 flex-col items-center text-center">
                            <p className="text-blue-400 text-2xl pb-4">LOREM IPSUM TITULIM</p>
                            <p className="text-gray-400 text-base font-light">
                                Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum
                            </p>
                        </div>
                        <div className="flex flex-wrap m-4">
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <div>
                                    <div className="flex flex-wrap justify-center ">
                                        <div className="rounded-full w-40 h-40 flex items-center justify-center bg-blue-400 mx-auto ">
                                            <img src="public/assets/img/elements/cotiza.png" alt="..." className="p-1"
                                                style={{ maxWidth: "120px" }} />
                                        </div>
                                    </div>
                                    <p className="leading-relaxed text-gray-400 text-center p-6">Lorem Ipsum</p>
                                </div>
                            </div>
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <div>
                                    <div className="flex flex-wrap justify-center ">
                                        <div className="rounded-full w-40 h-40 flex items-center justify-center mx-auto" style={{ "backgroundColor": "rgba(5,134 ,142, var(--tw-bg-opacity))" }}>
                                            <img src="public/assets/img/elements/clock.png" alt="..." className="p-1"
                                                style={{ maxWidth: "120px" }} />
                                        </div>
                                    </div>
                                    <p className="leading-relaxed text-gray-400 text-center p-6">Lorem Ipsum</p>
                                </div>
                            </div>
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <div>
                                    <div className="flex flex-wrap justify-center ">
                                        <div className="rounded-full w-40 h-40 flex items-center justify-center bg-yellow-300 mx-auto">
                                            <img src="public/assets/img/elements/money.png" alt="..." className="p-1"
                                                style={{ maxWidth: "120px" }} />
                                        </div>
                                    </div>
                                    <p className="leading-relaxed text-gray-400 text-center p-6">Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <section className="bg-gray-200 pb-32">
                    <div className="max-w-screen-lg mx-auto ">
                        <div className="flex flex-wrap w-full flex-col items-center text-center">
                            <p className="text-blue-400 text-2xl pb-2 pt-12 font-medium">¿POR QUÉ USAR LOREM IPSUM?</p>
                        </div>
                        <div className="flex flex-wrap m-4">
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <p className="leading-relaxed text-blue-400 text-left font-medium">PRIMERO</p>
                                <label className="font-normal text-gray-400">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar</label>
                            </div>
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <p className="leading-relaxed text-blue-400 text-left font-medium">SEGUNDO</p>
                                <label className="font-normal text-gray-400">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar</label>
                            </div>
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <p className="leading-relaxed text-blue-400 text-left font-medium">TERCERO</p>
                                <label className="font-normal text-gray-400">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar</label>
                            </div>
                        </div>
                        <div className="flex flex-wrap m-4">
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <p className="leading-relaxed text-blue-400 text-left font-medium">CUARTO</p>
                                <label className="font-normal text-gray-400">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar</label>
                            </div>
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <p className="leading-relaxed text-blue-400 text-left font-medium">QUINTO</p>
                                <label className="font-normal text-gray-400">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar</label>
                            </div>
                            <div className="xl:w-1/3 md:w-1/3 sm:w-1/3 w-full p-4">
                                <p className="leading-relaxed text-blue-400 text-left font-medium">SEXTO</p>
                                <label className="font-normal text-gray-400">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar</label>
                            </div>
                        </div>
                    </div>
                </section>
                <section className=" bg-white pb-32">
                    <div className="max-w-screen-lg mx-auto">
                        <div className="flex flex-wrap w-full flex-col items-center">
                            <p className="text-blue-400 text-2xl pb-2 pt-12 font-medium text-center">BENEFICIOS DE UTILIZAR LOREM IPSUM</p>
                            <div className="flex items-start ">
                                <img className="py-8 flex-none w-6 h-full" src="public/assets/img/elements/check.png"></img>
                                <span className="font-normal text-lg text-gray-400 text-left p-4">Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</span>
                            </div>
                            <div className="flex items-start ">
                                <img className="py-8 flex-none w-6 h-full" src="public/assets/img/elements/check.png"></img>
                                <span className="font-normal text-lg text-gray-400 text-left p-4">El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de usar textos como por ejemplo "Contenido aquí, contenido aquí".</span>
                            </div>
                            <div className="flex items-start ">
                                <img className="py-8 flex-none w-6 h-full" src="public/assets/img/elements/check.png"></img>
                                <span className="font-normal text-lg text-gray-400 text-left p-4"> Estos textos hacen parecerlo un español que se puede leer. Muchos paquetes de autoedición y editores de páginas web usan el Lorem Ipsum como su texto por defecto, y al hacer una búsqueda de "Lorem Ipsum" va a dar por resultado muchos sitios web que usan este texto si se encuentran en estado de desarrollo. Muchas versiones han evolucionado a través de los años, algunas veces por accidente, otras veces a propósito (por ejemplo insertándole humor y cosas por el estilo).</span>
                            </div>
                            <button className="bg-green-500 hover:bg-green-600 text-white font-normal text-2xl w-4/12  py-2 mt-12 rounded">COTIZA</button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    );
}

export default Faq;