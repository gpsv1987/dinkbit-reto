import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFacebookF, faTwitter, faLinkedinIn} from '@fortawesome/free-brands-svg-icons';
export default function Footer() {
  return (
    <footer className="relative ">
        <div className="mx-auto bg-gray-200 p-2 md:justify-between justify-left">
          <div className="max-w-screen-lg w-full text-left mx-auto">
            <div className="text-base text-gray-400 font-normal ">
              <a
                className="hover:text-gray-500 pr-4"
              >
                QUIÉNES SOMOS
              </a>
              <a
                className="hover:text-gray-500 p-4"
              >
                NUESTROS SERVICIOS
              </a>
              <a
                className="hover:text-gray-500 p-4"
              >
                FAQ
              </a>
              <a
                className="hover:text-gray-500 p-4"
              >
                CONTACTO
              </a>
              <a
                className="hover:text-gray-500 p-4"
              >
                COTIZA TU PRÉSTAMO
              </a>
            </div>
          </div>
          <div className="max-w-screen-lg flex w-full mx-auto">
            <div className="w-4/12 text-sm text-gray-400 font-semibold py-4 pr-4 text-left">
              <p><span>&#169;</span>2015 dinkbit <br></br>Todos los derechos reservados</p>
            </div>
            <div className="w-4/6 text-sm text-gray-400 font-semibold py-4 text-left">
              <p>Aviso de privacidad</p>
            </div>
          </div>
        </div>
        <div className="mx-auto bg-blue-900 p-2 md:justify-between justify-left ">
          <div className="max-w-screen-lg flex flex-wrap w-full px-4 mx-auto mx-auto">
            <div className="text-sm text-white font-semibold py-1 text-left w-6/12">
              <a
                className="hover:text-gray-500 px-1"
              >
                ¿Preguntas? 
              </a>|
              <a
                className="hover:text-gray-500 px-1"
              >
                contacto@dinkbit.com 
              </a>|
              <a
                className="hover:text-gray-500 px-1"
              >
                2224 1607
              </a>
            </div>
            <div className="text-sm text-white font-semibold py-1 text-right w-6/12">
              <label
                className="hover:text-gray-500"
              >
                SÍGUENOS EN: 
              </label>
              <a
                className="hover:text-gray-500 px-3 text-white" 
              >
                <FontAwesomeIcon icon={faFacebookF } />
              </a>
              <a
                className="hover:text-gray-500 px-3 text-white" 
              >
                <FontAwesomeIcon icon={faTwitter } />
              </a>
              <a
                className="hover:text-gray-500 px-3 text-white" 
              >
                <FontAwesomeIcon icon={ faLinkedinIn } />
              </a>
              <a
                className="hover:text-gray-500 px-3 text-white" 
              >
                  <img className="h-4 fill-current inline" src="public/assets/img/logos/logo.png" viewBox="0 0 512.005 512.005"></img>
              </a>
            
            </div>
          </div>
        </div>
    </footer>
  );
}

