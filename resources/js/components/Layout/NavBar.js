import React, { useState, Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';
const NavBar = () => {
  const [active, setActive] = useState(false);
  const onClick = () => {
    setActive(!active);
  };
  return (
    <nav id="header" className="fixed w-full z-30 top-0 text-white">
      <div className="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 py-2">
        <div className="pl-4 flex items-center pb-2">
          <Link to={'/quote/'} className="toggleColour text-white no-underline hover:no-underline font-bold text-2xl lg:text-4xl">
            <img className="h-8 fill-current inline" src="public/assets/img/logos/logo.png" viewBox="0 0 512.005 512.005">
            </img>
          </Link>
        </div>
        <div className="block lg:hidden pr-4">
          <button id="nav-toggle" className="flex items-center p-1 text-white focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">
            <svg className="fill-current h-6 w-6" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
              <title>Menu</title>
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
            </svg>
          </button>
        </div>
        <div className="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden mt-2 lg:mt-0 bg-blue-900 lg:bg-transparent p-4 lg:p-0 z-20" id="nav-content">
          <ul className="list-reset lg:flex justify-end flex-1 items-center text-white" id="ulText">
            <li className="mx-1">
              <NavLink activeClassName="is-active" to={'/quote/About'}
                className="inline-block py-2 px-2 no-underline hover:underline font-bold mt-2 lg:mt-0 opacity-75 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out" >QUIÉNES SOMOS</NavLink>
            </li>
            <li className="mx-1">
              <NavLink activeClassName="is-active" to={'/quote/Services'} className="inline-block py-2 px-2 no-underline hover:underline font-bold mt-2 lg:mt-0 opacity-75 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">NUESTROS SERVICIOS</NavLink>
            </li>
            <li className="mx-1">
              <NavLink activeClassName="is-active" to={'/quote/Faq'} className="inline-block py-2 px-2 no-underline hover:underline font-bold mt-2 lg:mt-0 opacity-75 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">FAQ</NavLink>
            </li>
            <li className="mx-1">
              <NavLink activeClassName="is-active" to={'/quote/Contact'} className="inline-block py-2 px-2 no-underline hover:underline font-bold mt-2 lg:mt-0 opacity-75 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out">CONTACTO</NavLink>
            </li>
            <li className="mx-1">
              <NavLink activeClassName="is-active" to={'/quote/Qoute'} className="inline-block py-2 px-2 no-underline hover:underline font-bold mt-4 lg:mt-0 opacity-75 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-300 ease-in-out border border-white rounded-md">COTIZA UN PRÉSTAMO</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default NavBar;