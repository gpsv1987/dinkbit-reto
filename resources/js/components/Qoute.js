import React, { useState } from 'react';
import QouteApp from './QouteApp';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content'
import axios from 'axios';
import { useForm } from 'react-hook-form';


const Qoute = () => {
    let [loading, setLoading] = useState(false);
    let [color, setColor] = useState("#ffffff");
    const [user, setUser] = useState({
        name: '',
        surname: '',
        cellPhone: '',
        email: '',
        subject: '',
        company: '',
        officePhone: '',
        message: ''
    });
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const optionData = [
        { id: 1, title: 'Asunto 1' },
        { id: 2, title: 'Asunto 2' },
        { id: 3, title: 'Asunto 3' },
        { id: 4, title: 'Asunto 4' }
    ];

    const { name, surname, cellPhone, email, subject, company, officePhone, message } = user;
    const onChange = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    }
    const MySwal = withReactContent(Swal);
    const onSubmit = e => {
        Swal.fire({
            title: 'DinkBit',
            html: 'Enviando...',
            timer: 6000,
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getHtmlContainer()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => { })
        axios
            .post("/quote/send", user, {
                headers: {
                    _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                    headers: {
                        'XSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                    }
                }
            })
            .then(res => {
                MySwal.fire(
                    'Correo enviado'
                )
                if (res.data === 200)
                    setUser({
                        name: '',
                        surname: '',
                        cellPhone: '',
                        email: '',
                        subject: '',
                        company: '',
                        officePhone: '',
                        message: ''
                    });
            }
            ).catch(err => Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo salió mal en el envió de correo',
                footer: `<a href>${err}</a>`
            }));
        loading = false;
    }
    const logoCard = "public/assets/img/Headers/header-cotizador.jpg";

    return (
        <div className="min-h-full font-body">
            <div className="imageBack flex flex-col">
                <div className="h-screen bg-cover relative flex items-center justify-center h-full bg-center bg-no-repeat max-h-screen min-h-full"
                    style={{ backgroundImage: `url(${logoCard})` }} >
                    <div className="z-10 container text-center px-3 lg:px-0 ">
                        <p className="lg:text-3xl md:text-3xl text-2xl text-white font-semibold py-6">Lorem Ipsum</p>
                        <p className="leading-normal text-base md:text-2xl lg:text-2xl mb-8 text-white">
                            Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.<br></br> Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor
                        </p>
                    </div>
                </div>
                <section className=" text-gray-200 bg-white">
                    <div className="max-w-screen-lg mx-auto px-5 py-16 ">
                        <div className="flex flex-wrap">
                            <div className="xl:w-6/12 md:w-6/12 sm:w-6/12 w-full">
                                <div className="w-full pr-8">
                                    <p className="text-blue-400 text-2xl font-medium pb-3">LOREM IPSUM</p>
                                    <p className="text-gray-400 text-sm font-normal pt-3">Lorem Ipsum es simplemente el texto de relleno</p>
                                    <p className="text-gray-400 text-sm font-normal py-3">de las imprentas y archivos de texto. Lorem Ipsum ha sido el</p>
                                    <p className="text-gray-500 text-xl font-medium pt-5">LOREM IPSUM</p>
                                    <p className="text-gray-400 text-sm font-normal pt-3">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.</p>
                                    <p className="text-gray-500 text-xl font-medium pt-5">LOREM IPSUM</p>
                                    <p className="text-gray-400 text-sm font-normal pt-3">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.</p>
                                    <p className="text-gray-400 text-sm font-normal pt-2">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.</p>
                                    <p className="text-gray-500 text-xl font-medium pt-5">LOREM IPSUM</p>
                                    <p className="text-gray-400 text-sm font-normal pt-3">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.</p>


                                </div>
                            </div>
                            <div className="xl:w-6/12 md:w-6/12 sm:w-6/12 w-full">
                                <QouteApp />
                            </div>
                        </div>
                        <hr></hr>
                        <section className=" bg-white pb-20 w-full">

                            <div className="max-w-screen-lg mx-auto">
                                <div className="flex flex-wrap w-full flex-col items-left">
                                    <div className="flex items-start ">
                                        <img className="py-6 flex-none w-3 h-full" src="public/assets/img/elements/sq-check-xs.png"></img>
                                        <span className="font-normal text-lg text-gray-400 text-left p-4">Lorem Ipsum <label className="text-blue-400">82.86%</label></span>
                                    </div>
                                    <div className="flex items-start ">
                                        <img className="py-6 flex-none w-3 h-full" src="public/assets/img/elements/sq-check-xs.png"></img>
                                        <span className="font-normal text-sm text-gray-400 text-left p-4">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.</span>
                                    </div><div className="flex items-start ">
                                        <img className="py-6 flex-none w-3 h-full" src="public/assets/img/elements/sq-check-xs.png"></img>
                                        <span className="font-normal text-sm text-gray-400 text-left p-4">Lorem <label className="text-blue-400">136.46%</label> Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto<label className="text-blue-400">01/10/2015</label></span>
                                    </div><div className="flex items-start ">
                                        <img className="py-6 flex-none w-3 h-full" src="public/assets/img/elements/sq-check-xs.png"></img>
                                        <span className="font-normal text-sm text-gray-400 text-left p-4">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.</span>
                                    </div><div className="flex items-start ">
                                        <img className="py-6 flex-none w-3 h-full" src="public/assets/img/elements/sq-check-xs.png"></img>
                                        <span className="font-normal text-sm text-gray-400 text-left p-4">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.</span>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>

            </div>
        </div>
    );
}

export default Qoute;