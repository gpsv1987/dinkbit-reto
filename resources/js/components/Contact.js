import React, { useState, Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faFacebookF, faLinkedinIn, faTwitter } from '@fortawesome/free-brands-svg-icons';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content'
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import GoogleMaps from 'simple-react-google-maps';

const Contact = () => {
    let [loading, setLoading] = useState(false);
    let [color, setColor] = useState("#ffffff");
    const [user, setUser] = useState({
        name: '',
        surname: '',
        cellPhone: '',
        email: '',
        subject: '',
        company: '',
        officePhone: '',
        message: ''
    });
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const optionData = [
        { id: 1, title: 'Asunto 1' },
        { id: 2, title: 'Asunto 2' },
        { id: 3, title: 'Asunto 3' },
        { id: 4, title: 'Asunto 4' }
    ];

    const { name, surname, cellPhone, email, subject, company, officePhone, message } = user;
    const onChange = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    }
    const MySwal = withReactContent(Swal);
    const onSubmit = e => {
        Swal.fire({
            title: 'DinkBit',
            html: 'Enviando...',
            timer: 6000,
            timerProgressBar: true,
            didOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getHtmlContainer()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => { })
        axios
            .post("/quote/send", user, {
                headers: {
                    _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                    headers: {
                        'XSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                    }
                }
            })
            .then(res => {
                MySwal.fire(
                    'Correo enviado'
                )
                if (res.data === 200)
                    setUser({
                        name: '',
                        surname: '',
                        cellPhone: '',
                        email: '',
                        subject: '',
                        company: '',
                        officePhone: '',
                        message: ''
                    });
            }
            ).catch(err => Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Algo salió mal en el envió de correo',
                footer: `<a href>${err}</a>`
            }));
        loading = false;
    }
    const logoCard = "public/assets/img/Headers/header-contacto.jpg";

    return (
        <div className="min-h-full font-body">
            <div className="imageBack flex flex-col">
                <div className="h-screen bg-cover relative flex items-center justify-center h-full bg-center bg-no-repeat max-h-screen min-h-full"
                    style={{ backgroundImage: `url(${logoCard})` }} >
                    <div className="z-10 container text-center px-3 lg:px-0 ">
                        <p className="lg:text-3xl md:text-3xl text-2xl text-white font-semibold py-6">Lorem Ipsum</p>
                        <p className="leading-normal text-base md:text-2xl lg:text-2xl mb-8 text-white">
                            Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.<br></br> Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor
                        </p>
                    </div>
                </div>
                <section className=" text-gray-200 bg-white">
                    <div className="max-w-screen-lg mx-auto px-5 py-24 ">
                        <div className="flex flex-wrap w-full mb-20 flex-col text-left">
                            <p className="text-blue-400 text-2xl py-2">LOREM IPSUM</p>
                            <p className="text-gray-400 text-base font-light">
                                Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.<br></br>
                            Lorem Ipsum es simplemente el texto <label className="text-blue-400">30</label>Lorem Ipsum es simplemente el texto de relleno <label className="text-blue-400">Sólo dinos.</label>
                            </p>
                        </div>

                        <div className="flex flex-wrap">
                            <div className="xl:w-6/12 md:w-6/12 sm:w-6/12 w-full">
                                <form
                                    className="w-full text-left"
                                    onSubmit={handleSubmit(onSubmit)}
                                >
                                    <div className="flex flex-wrap mb-6">
                                        <div className="w-full mb-6 md:mb-0">
                                            <input
                                                {...register('name', { required: true, maxLength: 25, minLength: 3 })}
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                                id="name"
                                                name="name"
                                                type="text"
                                                placeholder="Nombre"
                                                value={name}
                                                onChange={onChange}
                                            />{errors.name && <p className="text-red-300">El nombre es obligatorio</p>}
                                            <input
                                                {...register('surname', { required: true, maxLength: 25, minLength: 3 })}
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                                id="surname"
                                                name="surname"
                                                type="text"
                                                placeholder="Apellidos"
                                                value={surname}
                                                onChange={onChange}
                                            />{errors.surname && <p className="text-red-300">El Apellido es obligatorio</p>}
                                            <input
                                                {...register('cellPhone', { required: true, maxLength: 15, minLength: 10 })}
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                                id="cellPhone"
                                                name="cellPhone"
                                                type="number"
                                                placeholder="Télefono casa o celular"
                                                value={cellPhone}
                                                onChange={onChange}
                                            />{errors.cellPhone && <p className="text-red-300">El télefono es obligatorio</p>}
                                            <input
                                                {...register('email', {
                                                    required: true,
                                                    pattern: {
                                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                        message: "Correo invalido"
                                                    }
                                                })}
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                                id="email"
                                                name="email"
                                                type="email"
                                                placeholder="Correo electrónico"
                                                value={email}
                                                onChange={onChange}
                                            />{errors.email && <p className="text-red-300">{errors.email.message}</p>}
                                            <select
                                                {...register('subject', { required: true })}
                                                onChange={onChange}
                                                value={subject}
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white">
                                                <option
                                                    value="" >Asunto (Elige Uno)</option>
                                                {optionData.map(user => {
                                                    return <option
                                                        key={user.id}
                                                        value={user.title}
                                                    >{user.title}
                                                    </option>;
                                                })}
                                            </select>{errors.subject && <p className="text-red-300">El asunto es obligatorio</p>}

                                            <input
                                                {...register('company', { required: true, maxLength: 25, minLength: 3 })}
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                                id="company"
                                                name="company"
                                                type="text"
                                                placeholder="Empresa"
                                                value={company}
                                                onChange={onChange}
                                            />{errors.company && <p className="text-red-300">La empresa es obligatoria</p>}
                                            <input
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                                id="officePhone"
                                                name="officePhone"
                                                type="number"
                                                placeholder="Télefono Oficina"
                                                value={officePhone}
                                                onChange={onChange}
                                            />
                                            <textarea
                                                {...register('message', { required: true, maxLength: 150, minLength: 3 })}
                                                className="appearance-none block w-full bg-gray-100 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                                id="message"
                                                name="message"
                                                type="text"
                                                placeholder="Mensaje"
                                                value={message}
                                                onChange={onChange}
                                            />{errors.message && <p className="text-red-300">El mensaje es obligatorio</p>}
                                            <div className="text-center py-2">
                                                <button
                                                    type="submint"
                                                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-full text-center"
                                                >ENVIAR</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="xl:w-6/12 md:w-6/12 sm:w-6/12 w-full">
                                <div className="w-full">
                                    <p className="text-blue-400 text-xl font-medium pl-4 pb-3">LOREM IPSUM</p>
                                    <p className="text-gray-400 text-sm font-medium pl-4">Bosque de Ciruelas #130 int. 1201
                                    <br></br>Col. Bosque de las Lomas. Cuajimalpa de Morelos
                                    <br></br>México D.F. C.P. 11700</p>
                                    <p className="text-sm pl-4 pt-4 text-gray-400">
                                        <FontAwesomeIcon icon={faPhoneAlt} />
                                        <label className="pl-4">2224160</label>
                                    </p>
                                    <p className="text-sm pl-4 py-3 text-gray-400">
                                        <FontAwesomeIcon icon={faEnvelope} />
                                        <label className="pl-4 py4">contacto@dinkbit.com</label>
                                    </p>
                                </div>
                                <div className="w-full">
                                    <p className="text-blue-400 text-xl font-medium pl-4 pt-4">DINKBIT</p>
                                    <p className="text-sm pl-4 pt-4 text-gray-400">
                                        <FontAwesomeIcon icon={faPhoneAlt} />
                                        <label className="pl-4">22241607</label>
                                    </p>
                                    <p className="text-sm pl-4 py-3 text-gray-400">
                                        <FontAwesomeIcon icon={faEnvelope} />
                                        <label className="pl-4 py4">hacemoscosasincreibles@dinkbit.com</label>
                                    </p>
                                </div>
                                <div className="w-full">
                                    <p className="text-blue-400 text-xl font-medium pl-4 pt-4">SÍGUENOS EN:</p>
                                    <a
                                        className="hover:text-gray-500 text-3xl px-3 text-gray-400"
                                    >
                                        <FontAwesomeIcon icon={faFacebookF} />
                                    </a>
                                    <a
                                        className="hover:text-gray-500 text-3xl px-3 text-gray-400"
                                    >
                                        <FontAwesomeIcon icon={faTwitter} />
                                    </a>
                                    <a
                                        className="hover:text-gray-500 text-3xl px-3 text-gray-400"
                                    >
                                        <FontAwesomeIcon icon={faLinkedinIn} />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <GoogleMaps
                            apiKey={"AIzaSyAT5BnyXVmO7quzuyZtMKXJ9OM2WuylPY4"}
                            style={{ height: "40rem" }}
                            zoom={15}
                            center={{ lat: 19.4209891, lng: -99.2466552 }}
                            markers={{ lat: 19.4209891, lng: -99.2466552 }}
                            className="max-w-screen-lg"
                        />
                    </div>

                </section>

            </div>
        </div>
    );
}

export default Contact;