import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';
import Services from './components/Services';
import Faq from './components/Faq';
import Contact from './components/Contact';
import Qoute from './components/Qoute';
import NavBar from './components/Layout/NavBar';
import Footer from './components/Layout/Footer';
function App() {
    return (
        <div >

            <Router>
                <NavBar/>
                <Switch>
                    <Route exact path="/quote/" component={Home} />
                    <Route exact path="/quote/About" component={About} />
                    <Route exact path="/quote/Services" component={Services} />
                    <Route exact path="/quote/Faq" component={Faq} />
                    <Route exact path="/quote/Contact" component={Contact} />
                    <Route exact path="/quote/Qoute" component={Qoute} />
                </Switch>
            </Router>
            <Footer />
        </div>
    );
}

export default App;