<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="96x96" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>DinkBit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.5, user-scalable=1">

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="{{ asset('public/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/Mycss.css') }}">
</head>
<body >
<div id="root"></div>
{{ csrf_field() }}
</body>
<script src="{{ asset('public/js/index.js') }}"></script>
<script>      
      let scrollpos = window.scrollY;
      let header = document.getElementById("header");
      let navcontent = document.getElementById("nav-content");
      let brandname = document.getElementById("brandname");
      let toToggle = document.querySelectorAll(".toggleColour");
      document.addEventListener("scroll", function () {
        /*Apply classes for slide in bar*/
        scrollpos = window.scrollY;
        if (scrollpos > 700) {
          header.classList.add("bg-blue-900");
          //Use to switch toggleColour colours
          for (var i = 0; i < toToggle.length; i++) {
            toToggle[i].classList.add("text-gray-800");
            toToggle[i].classList.remove("text-black");
          }
          header.classList.add("shadow");
        } else {
          header.classList.remove("bg-blue-900");
          //Use to switch toggleColour colours
          for (var i = 0; i < toToggle.length; i++) {
            toToggle[i].classList.add("text-black");
            toToggle[i].classList.remove("text-gray-800");
          }
          header.classList.remove("shadow");
        }
      });

      var navMenu = document.getElementById("nav-toggle");
      document.onclick = check;
      function check(e) {
        var target = (e && e.target) || (event && event.srcElement);

        //Nav Menu
        if (!checkParent(target, navcontent)) {
          // click NOT on the menu
          if (checkParent(target, navMenu)) {
            // click on the link
            if (navcontent.classList.contains("hidden")) {
              navcontent.classList.remove("hidden");
            } else {
              navcontent.classList.add("hidden");
            }
          } else {
            // click both outside link and outside menu, hide menu
            navcontent.classList.add("hidden");
          }
        }
      }
      function checkParent(t, elm) {
        while (t.parentNode) {
          if (t == elm) {
            return true;
          }
          t = t.parentNode;
        }
        return false;
      }</script>
</html>