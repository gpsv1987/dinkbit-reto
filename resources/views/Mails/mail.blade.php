<!DOCTYPE html>
<html lang="es">

<head>
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
</head>
<div>
  <div class="container text-center">
    <h1>DINKBIT Formulario</h1>
  </div>
  <div class="container">
    <div class="table-wrapper">
      <p class="card-title">Nombre: <label class="card-text">{{ $msg['name']}}</label></p>
      <p class="card-title">surname: <label class="card-text">{{ $msg['surname']}}</label></p>
      <p class="card-title">cellPhone: <label class="card-text">{{ $msg['cellPhone']}}</label></p>
      <p class="card-title">subject: <label class="card-text">{{ $msg['subject']}}</label></p>
      <p class="card-title">company: <label class="card-text">{{ $msg['company']}}</label></p>
      <p class="card-title">officePhone: <label class="card-text">{{ $msg['officePhone']}}</label></p>
      <p class="card-title">message: <label class="card-text">{{ $msg['message']}}</label></p>
    </div>
  </div>
</div>

</html>